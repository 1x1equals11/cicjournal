<?php
  require 'database/db_connect.php';
  require 'database/db.php';
  require 'controllers/login_controller.php';
  require 'controllers/settings_controller.php';
  session_start();

  if(isset($_SESSION['id_number']) && $_SESSION['user_type'] == "SUPER"){
    header("location: admin/");
  }elseif (isset($_SESSION['id_number']) && $_SESSION['user_type'] == "STUDENT") {
    header("location: student/");
  }

  $login = new Login();
  $setting = new Settings();

 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="images/ciclogomain.png" type="image/png"/>

    <title>Home</title>

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">


</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content just">

                <!-- Topbar -->
                <nav class="navbar navbar-expand-lg navbar-light bg-gradient-primary mb-5">
                  <div class="container">
                    <a class="navbar-brand" href="#">
                       <img src="images/ciclogo.png" class="img-fluid logo-width">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                  <div class="collapse navbar-collapse" id="navbarText">
                    <div class="navbar-nav mr-auto"></div>
                    <span class="form-inline">
                        <a href="login.php" class="zoom text-white nav-item nav-link">Sign in</a>
                        <a href="#" class="zoom text-white nav-item nav-link">About us</a>
                    </span>
                  </div>
                  </div>
                </nav>
                <!-- end of Topbar -->


                <!-- Begin Page Content -->
                <div class="container">
                  <!-- row1 -->

                  <div bgcolor="red" class="position"></div>
                   <div class="row mb-5">
                    <div class="col-xs-4 col-md-6 mr-md-4">
                        <!-- Tagline -->
                      <h1 class="mb-4">
                        Discover scientific knowledge and stay connected to the world of science
                      </h1>
                        <!-- login link -->
                        <a href="login.php" class="btn btn-primary  btn-lg p-4 mb-5">Continue
                        to site</a>
                    </div>

                    <div class="col-xs-8 col-md text-center">
                          <!-- Page Heading -->
                        <img src="images/poster1.jpg" class="img-fluid rounded " alt="Responsive Image" width="500">
                    </div>
                  </div>
                  <!-- end of row1 -->


                  <!-- <div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-sm modal-notify" role="document">

                      <div class="modal-content text-center">

                        <div class="modal-header d-flex justify-content-center bg-dark">
                          <p class="heading text-gray-200">Continue as</p>
                        </div>

                        <div class="modal-body p-3">
                            <a class="nav-link bg-light" href="login.php">
                              <i class="fas fa-user text-danger"></i>
                              <span class="text-gray-800">User</span>
                            </a>

                            <a class="nav-link bg-light" href="#">
                             <i class="far fa-user text-danger"></i>
                              <span class="text-gray-800">Guest</span>
                            </a>
                        </div>
                      </div>

                    </div>
                  </div> -->





                  <!-- row 2 -->
                  <div class="row mb-5">
                    <div class="col-xs-4 col-md-6 mr-md-4 mb-4">
                      <img src="images/poster2.png" class="img-fluid" alt="Responsive Image" width="600">
                    </div>

                    <div class="col-xs-8 col-md-5 ">
                          <div class="h2 mb-2 text-gray-800">
                            Discover Research
                          </div>
                          <form action="search.php" method="get">
                            <input type="search" id="form1" name="search_id" class="form-control bg-light small shadow mb-4" placeholder="Search articles..."
                            aria-label="Search" />
                          </form>

                            <!-- row 1 -->
                            <div class="d-flex flex-row">
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=18" class="btn btn-outline-primary rounded-pill p-3">Systems and Informatics</a>
                              </div>
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=14" class="btn btn-outline-primary rounded-pill p-3">Socio-Cultural</a>
                              </div>
                            </div>

                            <!-- row 2 -->
                            <div class="d-flex flex-row bd-highlight">
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=17" class="btn btn-outline-primary rounded-pill p-3">Internet of Things</a>
                              </div>
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=12" class="btn btn-outline-primary rounded-pill p-3">Sustainable Agriculture</a>
                              </div>
                            </div>

                            <!-- row 3 -->
                            <div class="d-flex flex-row bd-highlight">
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=13" class="btn btn-outline-primary rounded-pill p-3">Policy Studies</a>
                              </div>
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=11" class="btn btn-outline-primary rounded-pill p-3">Climate Change</a>
                              </div>
                            </div>

                            <div class="d-flex flex-row bd-highlight">
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=16" class="btn btn-outline-primary rounded-pill p-3">Emerging Trends</a>
                              </div>
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=19" class="btn btn-outline-primary rounded-pill p-3">Environmental Computings</a>
                              </div>
                            </div>

                            <div class="d-flex flex-row bd-highlight">
                              <div class="p-2 bd-highlight">
                                <a href="categories.php?category_id=15" class="btn btn-outline-primary rounded-pill p-3">Manufacturing and production</a>
                              </div>
                            </div>

                    </div>
                  </div>
                  <!-- end of row2 -->

                  <!-- row 3 -->
                  <div class="row">
                    <div class="col-xs-4 col-md-6 mr-md-4 mb-4">
                     <div class="h2 mb-2 text-gray-800">
                          Measure your Impact
                        </div>
                        <div class="tagline mb-2 text-gray-600 text-break">
                          Get in-depth stats on who's been reading
                          your work and keep<br>track of your citations.
                        </div>
                        <div class="">
                          <img src="images/poster3.png" class="img-fluid" alt="Responsive Image" width="400">
                        </div>
                    </div>

                    <div class="col-xs-8 col-md mb-5">
                        <div class="h2 mb-2 text-gray-800">
                          Connect with your Scientific community
                        </div>
                        <div class="tagline mb-2 text-gray-600 text-break">
                          Share your research, collaborate with your peers, and get
                          the support you need to advance your career.
                        </div>
                        <div class="">
                          <img src="images/poster4.png" class="img-fluid" alt="Responsive Image" width="450  ">
                        </div>
                    </div>
                  </div>
                  <!-- end of row3 -->
                </div>
                <!-- /.container -->

                <div class="container-fluid">
                  <div class="row justify-content-center">
                    <div class="col-12 text-center">
                          <hr class="shadow-sm">
                    <h1 class="display-4">
                      It's important to save pieces of the past
                    </h1>
                    <h1 class="display-2  ">
                      <a href="">
                        Connect to us
                      </a>
                      <sup class="suptag">ONE-CIC-CONNECT</sup>
                    </h1>
                    </div>
                  </div>
                </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; CIC Journal 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>
