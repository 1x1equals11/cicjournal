-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 30, 2021 at 07:43 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cicjournal`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(100) NOT NULL,
  `activity` text NOT NULL,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=353 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `id_number`, `activity`, `date_time`) VALUES
(291, '2016-00361', 'EDIT USER ID_NUM(2015-00362)', '2021-07-04 03:37:35'),
(290, '2016-00361', 'EDIT USER ID_NUM(2015-00362)', '2021-07-04 03:30:51'),
(289, '2016-00361', 'EDIT USER ID_NUM(2015-00362)', '2021-07-04 03:27:45'),
(288, '2016-00361', 'EDIT USER ID_NUM(2015-00362)', '2021-07-04 03:27:20'),
(287, '2016-00361', 'EDIT USER ID_NUM(2015-00362)', '2021-07-03 20:51:36'),
(286, '2016-00361', 'EDIT USER ID_NUM(2015-00362)', '2021-07-03 20:49:17'),
(285, '2016-00361', 'EDIT USER ID_NUM(2015-19621)', '2021-07-03 20:48:16'),
(284, '2016-00361', 'ADDED NEW USER ID_NUM(2015-00362)', '2021-07-03 20:43:29'),
(283, '2016-00361', 'ADDED NEW USER ID_NUM(2015-00036)', '2021-07-03 20:34:52'),
(282, '2016-00361', 'EDIT PAPER POST_ID(59)', '2021-07-03 13:16:54'),
(281, '2016-00361', 'EDIT PAPER POST_ID(59)', '2021-07-03 13:16:29'),
(280, '2016-00361', 'EDIT PAPER POST_ID(59)', '2021-07-03 13:15:46'),
(279, '2016-00361', 'EDIT PAPER POST_ID(59)', '2021-07-03 13:14:59'),
(278, '2016-00361', 'EDIT PAPER POST_ID(59)', '2021-07-03 13:14:21'),
(277, '2016-00361', 'ADDED NEW PAPER', '2021-07-03 13:02:28'),
(276, '2016-00361', 'ADDED NEW PAPER', '2021-07-03 12:59:20'),
(275, '2016-00361', 'DELETED PAPER POST_ID(57)', '2021-07-01 18:07:57'),
(274, '2016-00361', 'DELETED PAPER POST_ID(55)', '2021-07-01 18:07:51'),
(273, '2016-00361', 'LOGGED IN', '2021-07-01 16:16:17'),
(272, '2016-00361', 'LOGGED OUT', '2021-07-01 15:15:08'),
(271, '2016-00361', 'LOGGED OUT', '2021-06-30 16:54:55'),
(270, '2016-00361', 'EDIT PAPER POST_ID(57)', '2021-06-30 16:53:02'),
(269, '2016-00361', 'EDIT PAPER POST_ID(57)', '2021-06-30 16:52:53'),
(268, '2016-00361', 'ADDED NEW PAPER', '2021-06-30 16:31:26'),
(267, '2016-00361', 'DELETED PAPER POST_ID(56)', '2021-06-30 16:30:48'),
(266, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:17:44'),
(265, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:17:35'),
(264, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:17:03'),
(263, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:16:59'),
(262, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:16:30'),
(261, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:16:16'),
(260, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:15:39'),
(259, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:13:20'),
(258, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:13:14'),
(257, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:12:54'),
(255, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:11:46'),
(256, '2016-00361', 'EDIT PAPER POST_ID(54)', '2021-06-30 16:12:07'),
(317, '2016-00361', 'LOGGED OUT', '2021-07-27 20:11:11'),
(318, '2016-00361', 'LOGGED IN', '2021-07-27 20:13:30'),
(319, '2016-00361', 'LOGGED OUT', '2021-07-27 20:17:04'),
(320, '2016-00361', 'LOGGED IN', '2021-07-27 20:17:13'),
(321, '2016-00361', 'LOGGED OUT', '2021-07-27 20:17:29'),
(322, '2016-00361', 'LOGGED IN', '2021-07-27 20:17:33'),
(323, '2016-00361', 'LOGGED OUT', '2021-07-28 05:04:40'),
(324, '2016-00361', 'LOGGED IN', '2021-07-29 15:12:07'),
(325, '2016-00361', 'LOGGED OUT', '2021-07-29 15:35:27'),
(326, '2016-00361', 'LOGGED IN', '2021-07-29 16:16:59'),
(327, '2016-00361', 'LOGGED OUT', '2021-07-29 16:18:53'),
(328, '2016-00361', 'LOGGED IN', '2021-07-29 20:45:43'),
(329, '2016-00361', 'LOGGED OUT', '2021-07-29 20:46:33'),
(330, '2016-00361', 'LOGGED IN', '2021-07-29 20:48:53'),
(331, '2016-00361', 'LOGGED OUT', '2021-07-29 21:04:44'),
(332, '2016-00361', 'LOGGED IN', '2021-07-29 21:17:46'),
(333, '2016-00361', 'LOGGED OUT', '2021-07-29 22:30:30'),
(334, '2016-00361', 'LOGGED IN', '2021-07-29 22:30:35'),
(335, '2016-00361', 'EDIT USER ID_NUM(2015-19621)', '2021-07-29 23:33:41'),
(336, '2016-00361', 'LOGGED OUT', '2021-07-30 05:14:02'),
(337, '2016-00361', 'LOGGED IN', '2021-07-30 08:28:07'),
(338, '2016-00361', 'LOGGED OUT', '2021-07-30 08:28:14'),
(339, '2016-00361', 'LOGGED IN', '2021-07-30 08:28:18'),
(340, '2016-00361', 'LOGGED OUT', '2021-07-30 08:28:23'),
(341, '2016-00361', 'LOGGED IN', '2021-07-30 08:37:49'),
(342, '2016-00361', 'LOGGED OUT', '2021-07-30 08:38:04'),
(343, '2016-00361', 'LOGGED IN', '2021-07-30 08:49:26'),
(344, '2016-00361', 'ADDED NEW PAPER', '2021-07-30 11:55:33'),
(345, '2016-00361', 'EDIT PAPER POST_ID(60)', '2021-07-30 12:39:46'),
(346, '2016-00361', 'LOGGED OUT', '2021-07-30 12:49:52'),
(347, '2016-00361', 'LOGGED IN', '2021-07-30 12:50:20'),
(348, '2016-00361', 'LOGGED OUT', '2021-07-30 12:51:13'),
(349, '2016-00361', 'LOGGED IN', '2021-07-30 15:01:23'),
(350, '2016-00361', 'ADDED NEW PAPER', '2021-07-30 15:09:15'),
(351, '2016-00361', 'LOGGED OUT', '2021-07-30 15:38:29'),
(352, '2016-00361', 'LOGGED IN', '2021-07-30 15:40:28');

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `id_number` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`) VALUES
(15, 'Manufacturing And Production'),
(14, 'Socio Cultural'),
(13, 'Policy Studies'),
(12, 'Sustainable Agriculture'),
(11, 'Climate Change'),
(16, 'Emerging Trends'),
(17, 'Internet Of Things'),
(18, 'Systems And Informatics'),
(19, 'Environmental Computing');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `id_number` varchar(30) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'VISIBLE',
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `name`) VALUES
(13, 'Bachelor of Science in Information Security'),
(12, 'Bachelor of Science in Information Technology'),
(14, 'Bachelor of Science in Computer Science'),
(16, 'Bacehelor Of Science In Library Idk'),
(17, 'Bachelor Of Test');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(60) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`id_number`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user_id`, `id_number`, `user_type`, `password`) VALUES
(26, '2016-00361', 'SUPER', '$2y$10$t8dFx.haPyWPofxTM5oC9OTCH0WCTzlzR36Czkbe3qbpJt/ezODdm'),
(27, '2015-19621', 'STUDENT', '$2y$10$TyHV/63EbNZ1sh1x0G.Rh.uiPEsjH6GF3usdLn8u09idDZcPGozK2'),
(51, '2015-19620', 'STUDENT', '$2y$10$HJVQ1oiKOZBKyze9dO5N7e2OhaMv/.dNDx5F3w0ki.CjamB3BPKkm'),
(52, '2015-00036', 'STUDENT', '$2y$10$t8dFx.haPyWPofxTM5oC9OTCH0WCTzlzR36Czkbe3qbpJt/ezODdm'),
(53, '2015-00362', 'STUDENT', '$2y$10$maSep1lChLiDItGSupHn.Oo3UO5gRjPHG7uCgkTKAbhgxUW.9kKPW');

-- --------------------------------------------------------

--
-- Table structure for table `major`
--

DROP TABLE IF EXISTS `major`;
CREATE TABLE IF NOT EXISTS `major` (
  `major_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`major_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `major`
--

INSERT INTO `major` (`major_id`, `name`) VALUES
(10, 'Project Management'),
(9, 'Software Development');

-- --------------------------------------------------------

--
-- Stand-in structure for view `manage_user_view`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `manage_user_view`;
CREATE TABLE IF NOT EXISTS `manage_user_view` (
`id_number` varchar(60)
,`full_name` mediumtext
,`user_type` varchar(10)
,`status` varchar(25)
);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(300) NOT NULL,
  `title` varchar(250) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `year_publish` year(4) NOT NULL,
  `synopsis` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'ACTIVE',
  `upvotes` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(100) NOT NULL,
  `date_posted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `react`
--

DROP TABLE IF EXISTS `react`;
CREATE TABLE IF NOT EXISTS `react` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `id_number` varchar(30) NOT NULL,
  `react_type` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reg_info`
--

DROP TABLE IF EXISTS `reg_info`;
CREATE TABLE IF NOT EXISTS `reg_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(40) NOT NULL,
  `first_name` text,
  `middle_name` text,
  `last_name` text,
  `sex` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `prof_pic` varchar(100) DEFAULT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'ENABLED',
  `reg_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_number` (`id_number`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg_info`
--

INSERT INTO `reg_info` (`id`, `id_number`, `first_name`, `middle_name`, `last_name`, `sex`, `dob`, `prof_pic`, `status`, `reg_date`) VALUES
(1, '2016-00361', 'James Claude', 'Bongaitan', 'Lequin', 'MALE', '1998-10-04', NULL, 'ENABLED', '2021-06-12 11:38:47'),
(2, '2015-19621', 'Kimberly', 'Alegarme', 'Devocion', 'FEMALE', '1998-08-21', 'file-60dd2cdc8a8ea0.36322722.jpg', 'ENABLED', '2021-06-12 11:39:47'),
(27, '2015-00036', 'Royena Elise', 'Devocion', 'Lequin', 'FEMALE', '2020-10-03', NULL, 'ENABLED', '2021-07-03 20:34:52'),
(28, '2015-00362', 'James Benedict', 'Bongaitan', 'Lequin', 'MALE', '2006-03-14', NULL, 'ENABLED', '2021-07-03 20:43:29'),
(26, '2015-19620', 'Rueenie', 'Queenie', 'Dumangas', 'FEMALE', '1999-03-12', NULL, 'ENABLED', '2021-06-30 15:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(20) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `major_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_number` (`id_number`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `id_number`, `course_id`, `major_id`) VALUES
(7, '2015-19621', 12, 10),
(30, '2015-00362', 12, 10),
(28, '2015-19620', 12, 10),
(29, '2015-00036', 12, 10);

-- --------------------------------------------------------

--
-- Table structure for table `visitors_log`
--

DROP TABLE IF EXISTS `visitors_log`;
CREATE TABLE IF NOT EXISTS `visitors_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visited` varchar(30) NOT NULL,
  `visitor` varchar(30) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `manage_user_view`
--
DROP TABLE IF EXISTS `manage_user_view`;

DROP VIEW IF EXISTS `manage_user_view`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `manage_user_view`  AS  select `login`.`id_number` AS `id_number`,concat(`reg_info`.`first_name`,' ',`reg_info`.`middle_name`,' ',`reg_info`.`last_name`) AS `full_name`,`login`.`user_type` AS `user_type`,`reg_info`.`status` AS `status` from (`login` join `reg_info` on((`login`.`id_number` = `reg_info`.`id_number`))) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
