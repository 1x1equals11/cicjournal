<?php

class DBConnect
{
  //For local server credentials
  private $local_server = "localhost";
  private $local_username = "root";
  private $local_pass = "";
  private $local_db = "cicjournal";
  //Get Heroku ClearDB connection information
  private $cleardb_url = "";
  private $cleardb_server = "";
  private $cleardb_username = "";
  private $cleardb_password = "";
  private $cleardb_db = "";
  private $active_group = "";
  private $query_builder = "";

  public function __construct()
  {
    $this->cleardb_url = parse_url("mysql://b0f46f5fb91e41:d51188d3@us-cdbr-east-04.cleardb.com/heroku_6cb15404f0347e8?reconnect=true");
    $this->cleardb_server = $this->cleardb_url["host"];
    $this->cleardb_username = $this->cleardb_url["user"];
    $this->cleardb_password = $this->cleardb_url["pass"];
    $this->cleardb_db = substr($this->cleardb_url["path"],1);
    $this->active_group = 'default';
    $this->query_builder = TRUE;

    // Connect to DB
    $this->conn = mysqli_connect($this->cleardb_server, $this->cleardb_username, $this->cleardb_password, $this->cleardb_db);
    // $this->conn = mysqli_connect(
    //                 $this->local_server,
    //                 $this->local_username,
    //                 $this->local_pass,
    //                 $this->local_db);
    if(!$this->conn){
      die('MAINTENANCE LANG SA LODS');
    }else{

    }
  }

  public function getConnection()
  {
    return $this->conn;
  }
}




 ?>
