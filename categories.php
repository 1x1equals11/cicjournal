<?php
  require 'database/db_connect.php';
  require 'database/db.php';
  require 'controllers/settings_controller.php';
  require 'controllers/user_dashboard_controller.php';
  require 'controllers/posts_controller.php';
  require 'controllers/pagintation.php';
  session_start();

  $user = new UserDashboard("","");
  $post = new Posts("","");
  $setting = new Settings();
  $paginate = new Paginate(0);
  $category_name = "";

  if (isset($_GET['category_id'])) {
    $_GET['category_id'] = $paginate->sanitize($_GET['category_id']);
    $categories = $setting->getAllCategory();

    foreach ($categories as $category) {
      if ($category['category_id'] == $_GET['category_id']) {
        $category_name = $category['name'];
      }

    }
    // echo "asd:".$category_name;
    $new_array = $post->paginate($post->getAllPostByCategory($_GET['category_id']));
  }else{
    header('location: /cicjournal/');
  }
 ?>

 <!DOCTYPE html>
 <html lang="en">

 <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="shortcut icon" href="images/ciclogomain.png" type="image/png" />

     <title><?php echo  $category_name;?></title>

     <!-- Custom fonts for this template-->
     <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

     <!-- Custom styles for this template-->
     <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
     <link rel="stylesheet" href="assets/css/custom.css">

 </head>

 <body id="page-top">

     <!-- Page Wrapper -->
     <div id="wrapper">
       <!-- Content Wrapper -->
       <div id="content-wrapper" class="d-flex flex-column">
         <!-- Main Content -->
         <div id="content just">
           <!-- Topbar -->
           <nav class="navbar navbar-expand-lg navbar-light bg-gradient-primary mb-5">
             <div class="container">
               <a class="navbar-brand" href="/cicjournal/">
                  <img src="images/ciclogo.png" class="img-fluid logo-width">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
               </button>
             <div class="collapse navbar-collapse" id="navbarText">
               <div class="navbar-nav mr-auto"></div>
               <span class="form-inline">
                   <a href="login.php" class="zoom text-white nav-item nav-link">Sign in</a>
               </span>
             </div>
             </div>
           </nav>
           <!-- end of Topbar -->

           <!-- Begin Page Content -->
           <div class="continer">
             <div class="col mb-4 ml-1">
               <h4>Showing results for <b><?php echo $category_name; ?></b> </h4>
             </div>

             <?php if (!empty($new_array)): ?>
               <?php for($i=0; $i < sizeof($new_array); $i++){ ?>
                       <div id="<?php echo $i; ?>"style="text-align:center;<?php echo ($i==1 ? 'display:block;':'display:none;') ?>">
                         <button type='button' class='btn btn-dark btn-icon-split' onclick="show(<?php echo $i; ?>)">
                             <span class='icon text-white-50'>
                                 <i class='fas fa-chevron-circle-down'></i>
                             </span>
                             <span class='text'>Show more results</span>
                         </button>
                       </div>
                 <?php for($j=0; $j < sizeof($new_array[$i]); $j++){
                         $authors = $post->getPaperAuthors($new_array[$i][$j]['post_id']);
                         echo "<div class='col-lg mb-2'>
                             <div class='card ".$i."' style='width: auto; ".($i!=0 ? "display:none;":"display:block;")."'>
                               <div class='card-body'>
                                 <h5 class='card-title'><b>".strtoupper($new_array[$i][$j]['title'])."</b></h5>
                                 <h6 class='card-subtitle mb-2 text-muted'>";
                                 sizeof($authors);
                                 for ($k=0; $k < sizeof($authors); $k++) {
                                   $author_detail = $user->getSpecificUser($authors[$k]);
                                   echo $author_detail['first_name']." ".$author_detail['last_name'].", ";
                                 }
                                 echo $new_array[$i][$j]['year_publish']."</h6>
                                 <p class='card-text text-truncate'>".$new_array[$i][$j]['synopsis']."</p>
                                 <a href='viewpap.php?post_id=".$new_array[$i][$j]['post_id']."' class='btn btn-primary btn-icon-split'>
                                     <span class='icon text-white-50'>
                                         <i class='fas fa-info-circle'></i>
                                     </span>
                                     <span class='text'>Go to paper</span>
                                 </a>
                               </div>
                             </div>
                         </div>";
                      } ?>
               <?php } ?>
             <?php else: ?>
               <i class="ml-3">No result</i>
             <?php endif; ?>
           </div>

           <!-- Footer -->
           <footer class="sticky-footer bg-white">
               <div class="container my-auto">
                   <div class="copyright text-center my-auto">
                       <span>Copyright &copy; CIC Journal 2021</span>
                   </div>
               </div>
           </footer>
           <!-- End of Footer -->

       </div>
         <!-- End of Content Wrapper -->

     </div>
     <!-- End of Page Wrapper -->

     <!-- Scroll to Top Button-->
     <a class="scroll-to-top rounded" href="#page-top">
         <i class="fas fa-angle-up"></i>
     </a>

     <!-- Bootstrap core JavaScript-->
     <script src="assets/vendor/jquery/jquery.min.js"></script>
     <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

     <!-- Core plugin JavaScript-->
     <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

     <!-- Custom scripts for all pages-->
     <script src="assets/js/sb-admin-2.min.js"></script>

 </body>

 </html>
