<?php
  require '../database/db_connect.php';
  require '../database/db.php';

  $db = new DBConnect();
  $con = $db->getConnection();

  if (isset($_POST['fire'])) {
    // echo $_POST['test']."<br>";
    // $to_test = explode(" ",$_POST['test']);
    // $to_return = "";
    // echo sizeof($to_test);
    // for ($i=0; $i < sizeof($to_test); $i++) {
    //   if ($to_test[$i] == "In") {
    //     $to_test[$i] = "in";
    //   }elseif ($to_test[$i] == "Of") {
    //     $to_test[$i] = "of";
    //   }
    //   $to_return.=$to_test[$i];
    //   if ($i+1 != sizeof($to_test)) {
    //     $to_return.=" ";
    //   }
    // }
    //
    // echo '<br> Out: '.$to_return;
    // $out = "";
    // for ($i=0; $i < sizeof($exploded_string); $i++) {
    //   $exploded_string[$i] = ($i == 0 ? ucfirst($exploded_string[$i]) : strtolower($exploded_string[$i]));
    //   $out.=$exploded_string[$i];
    // }
    // echo $out;
    if (preg_match("/^[A-Za-z0-9:,'\-.() ]+$/",$_POST['test'])) {
      echo "match";
    }else{
      echo "not matchc";
    }

    // if (ctype_space($_POST['test'])) {
    //   echo "naay white";
    // }else{
    //   echo "wla";
    // }

    // if (ctype_punct($_POST['test'])) {
    //   echo "punctuation";
    // }else{
    //   echo "okayy sya";
    // }

  }

 ?>
 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
     <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
     <title>test</title>
   </head>
   <body>

     <form class="row g-3 needs-validation" novalidate>

       <div class="col-12">
         <label for="validationCustom02" class="form-label" >Last name</label>
         <input id="last_name" type="text" class="form-control" value="Otto" pattern="^([A-Z][a-z]+[\s]?)+$" oninput="check(this.value)" required>
         <div class="valid-feedback">
           Looks good!
         </div>
         <div id="last_name_error" class="invalid-feedback">
           Please choose a lastname.
         </div>
       </div> <br>

       <div class="col-12">
         <label for="validationCustomUsername" class="form-label">Username</label>
         <div class="input-group has-validation">
           <span class="input-group-text" id="inputGroupPrepend">@</span>
           <input id='user_name'type="email" class="form-control" aria-describedby="inputGroupPrepend" required>
           <div class="invalid-feedback">
             Please choose a username.
           </div>
         </div>
       </div> <br>

       <div class="col-12">
         <label for="validationCustom04" class="form-label">State</label>
         <select id='state' class="form-select" required>
           <option selected disabled value="">Choose...</option>
           <option>...</option>
         </select>
         <div class="invalid-feedback">
           Please select a valid state.
         </div>
       </div>

       <br>
       <div class="col-12">
         <button class="btn btn-primary" type="submit">Submit form</button>
       </div>
     </form>
     <script type="text/javascript">
       // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function () {
      // 'use strict'

      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      // var forms = document.querySelectorAll('.needs-validation')
      var form = document.getElementsByClassName('needs-validation');
      var last_name = document.getElementById('last_name');
      var user_name = document.getElementById('user_name');
      var state = document.getElementById('state');

      var last_name_error = document.getElementById('last_name_error');
      var flag = false;

      form[0].addEventListener('submit', function (e) {
        // Last name

        last_name.value = last_name.value.trim(); /^\s+$/.test(last_name.value)
        if (last_name.validity.patternMismatch) {
          last_name_error.innerHTML = "Last name should only be capitalized name";
          flag = false;
          // last_name_error.innerHTML = last_name.validationMessage;
        }
        //
        if (last_name.validity.valueMissing) {
          last_name.setCustomValidity("");
          last_name_error.innerHTML = last_name.validationMessage;
          flag = false;
        }

        if ("Baho" == last_name.value) {
          last_name.setCustomValidity(false);
          // last_name.setCustomValidity("This title is already in DB");
          last_name_error.innerHTML = "This title is already in DB";
          flag = false;
        }

        if (!flag) {
          e.preventDefault();
        }

        form[0].classList.add('was-validated');
      })

      // // Loop over them and prevent submission
      // Array.prototype.slice.call(forms)
      //   .forEach(function (form) {
      //     form.addEventListener('submit', function (event) {
      //       if (!form.checkValidity()) {
      //         event.preventDefault()
      //         // event.stopPropagation()
      //       }
      //
      //       form.classList.add('was-validated')
      //     }, false)
      //   })
      })()
     </script>
   </body>
 </html>
