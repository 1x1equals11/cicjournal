<?php
  require '../database/db_connect.php';
  require '../database/db.php';
  require '../controllers/user_dashboard_controller.php';
  require '../controllers/posts_controller.php';
  require '../controllers/settings_controller.php';
  session_start();

  if(!isset($_SESSION['id_number']) || $_SESSION['user_type'] != "SUPER"){
    header("location: ../");
  }

  if (!isset($_GET['post_id'])) {
    header("location: ../admin/");
  }

  $user = new UserDashboard($_SESSION['id_number'],$_SESSION['user_type']);
  $post = new Posts($_SESSION['id_number'],$_SESSION['user_type']);
  $setting = new Settings();
  $name = $user->getSpecificUser($_SESSION['id_number']);
  $view_post = $post->getSpecificPaper($_GET['post_id']);
  $likes = $post->getReactionCount($_GET['post_id'], "LIKE");
  $unlikes = $post->getReactionCount($_GET['post_id'], "UNLIKE");
  $comments = $post->getComments($_GET['post_id']);
  $_SESSION['user_name'] = ( $name != null ? $name['first_name']." ".$name['last_name'] : $_SESSION['id_number']);

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../images/ciclogomain.png" type="image/png"/>

    <title>Recent Paper</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <!-- Custom fonts for this template-->
    <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">

</head>

<body id="page-top" class="sidebar-toggled">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../admin/">
                <div class="sidebar-brand-text mr-auto">
                    <img src="../images/ciclogo.png" width="150">
                </div>
                <div class="sidebar-brand-icon">
                      <img src="../images/ciclogomain.png" width="50">
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Links
            </div>

            <li class="nav-item">
                <a class="nav-link" href="addpap.php">
                    <i class="fas fa-fw fa-file"></i>
                    <span>New Paper</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="managepap.php">
                    <i class="fas fa-fw fa-file-alt"></i>
                    <span>Manage Paper</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="manageuser.php">
                    <i class="fas fa-fw fa-user-plus"></i>
                    <span>User Management</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="studentsettings.php?view_type=course">
                    <i class="fas fa-fw fa-cogs"></i>
                    <span>Other Settings</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-dark topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                          method='GET'
                          action='sresults.php'>

                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search paper"
                                aria-label="Search" aria-describedby="basic-addon2" name='search_id' required>
                            <div class="input-group-append">
                              <button type='button' class="btn btn-info shadow-none" role="button"
                                  data-bs-toggle="dropdown" data-bs-auto-close="false" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-filter fa-sm"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                  aria-labelledby="userDropdown">
                                  <?php $categories = $setting->getAllCategory(); $counter = 0;?>
                                  <?php foreach ($categories as $category): ?>
                                    <a class="dropdown-item"><input type="radio" name="filter" value="<?php echo $category['category_id']; ?>" > <?php echo $category['name']; ?></a> <br>
                                  <?php $counter++; ?>
                                  <?php endforeach; ?>
                              </div>
                              <button type='submit' class="btn btn-primary" type="button">
                                  <i class="fas fa-search fa-sm"></i>
                              </button>
                            </div>
                        </div>

                    </form>



                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search"
                                  method='GET'
                                  action='sresults.php'>
                                  <div class="input-group">
                                      <input type="text" class="form-control bg-light border-0 small" placeholder="Search paper"
                                          aria-label="Search" aria-describedby="basic-addon2" name='search_id' required>
                                      <div class="input-group-append">
                                        <button type='button' class="btn btn-info shadow-none" role="button"
                                            data-bs-toggle="dropdown" data-bs-auto-close="false" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-filter fa-sm"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                            aria-labelledby="userDropdown">
                                            <?php $categories = $setting->getAllCategory(); $counter = 0;?>
                                            <?php foreach ($categories as $category): ?>
                                              <a class="dropdown-item"><input type="radio" name="filter" value="<?php echo $category['category_id']; ?>" > <?php echo $category['name']; ?></a> <br>
                                            <?php $counter++; ?>
                                            <?php endforeach; ?>
                                        </div>
                                        <button type='submit' class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                      </div>
                                  </div>
                                </form>
                            </div>
                        </li>


                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['user_name']?></span>
                                <img class="img-profile rounded-circle"
                                    src="../images/student-img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="account/activitylog.php">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <div class="container">
                    <div class="ml-4">
                        <div class="text-info">
                            <p class="small mb-2">
                              <?php
                                $date = date_create($view_post['date_posted']);
                                $date2 = date_format($date, "jS F, Y i:sa ");
                                echo $date2;
                              ?>
                                <!-- 28th January, 3:48pm -->
                            </p>
                             <h1 class="h3 mb-2"><?php echo $view_post['title']; ?></h1>

                        </div>
                        <div class="mb-2">
                            <code>Author/s</code>
                        </div>
                        <div class="row">
                          <?php $authors = $post->getPaperAuthors($view_post['post_id']); ?>
                           <?php if (!empty($authors)): ?>
                             <?php foreach ($authors as $value): ?>
                               <?php
                                $author = $user->getSpecificUser($value);
                                $default_avatar = ($author['sex'] == 'MALE'? 'male_default.jpg' : 'female_default.jpg' );
                                $author['prof_pic'] = ($author['prof_pic'] != null ? $author['prof_pic'] : $default_avatar);
                               ?>
                               <div class="d-flex align-items-center mr-4 mb-4" href="profile.php?id=<?php echo $author['id']; ?>">
                                   <div class="mr-2">
                                       <img class="icon_img rounded-circle" src="../profile_pictures/<?php echo $author['prof_pic']; ?>" width="40px">
                                   </div>
                                   <div class="font-weight-bold">
                                       <div class=""><?php echo $author['last_name'].", ".$author['first_name'] ?></div>
                                   </div>
                               </div>
                             <?php endforeach; ?>
                           <?php else: ?>
                             <i>Authors on paper</i>
                           <?php endif; ?>

                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 p-3">

                              <div class="d-sm-flex align-items-center justify-content-end">
                                  <!-- <br> -->
                                  <a href="../viewfile.php?file_name=<?php echo $view_post['file_name']; ?>" class="d-none mb-1 mt-1 d-inline-block btn btn-sm btn-primary shadow-sm" style="margin-right:10px;" target="_blank"><i class="fas fa-eye fa-sm text-white-50"></i> View File</a>
                                  <a href="../file_uploads/<?php echo $view_post['file_name']; ?>" class="d-none d-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download File</a>
                              </div>
                                <hr>
                                <h3 class="mb-4">Abstract</h3>
                                <p class="">
                                    <?php echo stripslashes($view_post['synopsis']); ?>
                                </p>
                                <hr>

                                <div id="comment_section" style="margin:1%;">
                                  <?php if (!empty($comments)): ?>
                                    <?php foreach ($comments as $comment): ?>
                                      <?php
                                        $keyboard_warrior = $user->getSpecificUser($comment['id_number']);
                                        $default_avatar = ($keyboard_warrior['sex'] == 'MALE'? 'male_default.jpg' : 'female_default.jpg' );
                                        $keyboard_warrior['prof_pic'] = ($keyboard_warrior['prof_pic'] != null ? $keyboard_warrior['prof_pic'] : $default_avatar);
                                      ?>
                                    <div class="card p-2">
                                      <div class="d-flex mr-4 mb-4 ">

                                        <div class="d-flex" id='user_comment_<?php echo $comment['id']; ?>'>
                                          <div class="mr-2">
                                              <div href="profile.php?id=<?php echo $keyboard_warrior['id']; ?>"><img class="icon_img rounded-circle" src="../profile_pictures/<?php echo $keyboard_warrior['prof_pic']; ?>" alt="" width="40px"></div>
                                          </div>
                                          <div class="font-weight-bold">
                                            <?php //$keyboard_warrior = $user->getSpecificUser($comment['id_number']); ?>
                                              <div href="profile.php?id=<?php echo $keyboard_warrior['id']; ?>"><div style="color:blue;"><?php echo $keyboard_warrior['first_name']." ".$keyboard_warrior['last_name']; ?></div></div>
                                              <?php echo stripslashes($comment['comment']); ?>
                                          </div>
                                        </div>

                                      </div>
                                      <div id='comment_date_<?php echo $comment['id']; ?>'>
                                        <small><i><?php echo $comment['comment_date']; ?></i></small>
                                      </div>

                                    </div>
                                    <br>
                                  <?php endforeach; ?>
                                  <?php else: ?>
                                    <i>No comments</i>
                                  <?php endif; ?>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; CIC Journal System 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="../logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../assets/js/sb-admin-2.min.js"></script>

</body>

</html>
