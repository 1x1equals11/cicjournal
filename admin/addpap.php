<?php
  require '../database/db_connect.php';
  require '../database/db.php';
  require '../controllers/settings_controller.php';
  require '../controllers/user_dashboard_controller.php';
  require '../controllers/posts_controller.php';
  session_start();

  $user = new UserDashboard($_SESSION['id_number'],$_SESSION['user_type']);
  $post = new Posts($_SESSION['id_number'],$_SESSION['user_type']);
  $setting = new Settings();
  $error = "";
  if(!isset($_SESSION['id_number']) && $_SESSION['user_type'] != "SUPER"){
    header("location: ../");
  }

 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../images/ciclogomain.png" type="image/png"/>

    <title>Add Papers</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <!-- Custom fonts for this template-->
    <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="..\assets\css\sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">

</head>

<body id="page-top" class="sidebar-toggled" >

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../admin/">
                <div class="sidebar-brand-text mr-auto">
                    <img src="../images/ciclogo.png" width="150">
                </div>
                <div class="sidebar-brand-icon">
                      <img src="../images/ciclogomain.png" width="50">
                </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-home"></i>
                    <span>Home</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Links
            </div>

            <li class="nav-item">
                <a class="nav-link" href="addpap.php">
                    <i class="fas fa-fw fa-file"></i>
                    <span>New Paper</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="managepap.php">
                    <i class="fas fa-fw fa-file-alt"></i>
                    <span>Manage Paper</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="manageuser.php">
                    <i class="fas fa-fw fa-user-plus"></i>
                    <span>User Management</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="studentsettings.php?view_type=course">
                    <i class="fas fa-fw fa-cogs"></i>
                    <span>Other Settings</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-dark topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                          method='GET'
                          action='sresults.php'>

                          <div class="input-group">
                              <input type="text" class="form-control bg-light border-0 small" placeholder="Search paper"
                                  aria-label="Search" aria-describedby="basic-addon2" name='search_id' required>
                              <div class="input-group-append">
                                <button type='button' class="btn btn-info shadow-none" role="button"
                                    data-bs-toggle="dropdown" data-bs-auto-close="false" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-filter fa-sm"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                    aria-labelledby="userDropdown">
                                    <?php $categories = $setting->getAllCategory(); $counter = 0;?>
                                    <?php foreach ($categories as $category): ?>
                                      <a class="dropdown-item"><input type="radio" name="filter" value="<?php echo $category['category_id']; ?>" > <?php echo $category['name']; ?></a> <br>
                                    <?php $counter++; ?>
                                    <?php endforeach; ?>
                                </div>
                                <button type='submit' class="btn btn-primary" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                              </div>
                          </div>

                    </form>



                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search"
                                  method='GET'
                                  action='sresults.php'>
                                  <div class="input-group">
                                      <input type="text" class="form-control bg-light border-0 small" placeholder="Search paper"
                                          aria-label="Search" aria-describedby="basic-addon2" name='search_id' required>
                                      <div class="input-group-append">
                                        <button type='button' class="btn btn-info shadow-none" role="button"
                                            data-bs-toggle="dropdown" data-bs-auto-close="false" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-filter fa-sm"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                            aria-labelledby="userDropdown">
                                            <?php $categories = $setting->getAllCategory(); $counter = 0;?>
                                            <?php foreach ($categories as $category): ?>
                                              <a class="dropdown-item"><input type="radio" name="filter" value="<?php echo $category['category_id']; ?>" > <?php echo $category['name']; ?></a> <br>
                                            <?php $counter++; ?>
                                            <?php endforeach; ?>
                                        </div>
                                        <button type='submit' class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                      </div>
                                  </div>
                                </form>
                            </div>
                        </li>

                        <!-- Nav Item - Alerts -->

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['user_name'] ?></span>
                                <img class="img-profile rounded-circle"
                                    src="../images/student-img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="account/activitylog.php">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content =  -->

                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row">
                      <div class="col-xl-12 col-lg-12">
                        <div class="card shadow mb-4 border-bottom-danger">
                          <div class="card-header py-3 d-flex flex-row align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary">Add Research Paper</h6>
                          </div>

                          <div class="card-body" >
                            <form id="add_form" class="needs-validation" method="post" enctype="multipart/form-data" novalidate >
                                <div class="form-group">
                                    <input id='title' type="text" class="form-control" placeholder="Title of the paper" name="title" oninput="validateTitle()" onfocus="validateTitle()">
                                    <div id='title_error' class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                  <textarea id='abstract' class="form-control" rows="8" cols="80" placeholder="Abstract" name="synopsis" oninput="validateAbstract()" onfocus="validateAbstract()"></textarea>
                                  <div id='abstract_error' class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                  <input class="form-control" id="year_publish" type="number" pattern="^[0-9]{4}$" name="year" placeholder="Year Published" oninput="validateYear()" onfocus="validateYear()">
                                  <div id='year_error' class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                  <select id="category" class="form-control" name="category" pattern="^(?:[1-9][0-9]+|[1-9]+)$" onchange="validateCategory()" onfocus="validateCategory()">

                                    <?php
                                      $categories = $setting->getAllCategory();
                                      $counter = 0;
                                      foreach ($categories as $category) {
                                        if ($counter == 0) {
                                          ?>
                                          <option selected hidden>Category</option>
                                          <?php
                                        }
                                        $counter ++;
                                        echo "<option value='".$category['category_id']."'>".$category['name']."</option>";
                                      }
                                     ?>
                                  </select>
                                  <div id='category_error' class="invalid-feedback"></div>
                                </div>
                                <div class="form-group" id="datalistgroup">

                                    <div class="input-group">
                                      <input type="text" class="form-control" id="datalistinput" pattern="^[1-9][0-9]{3}-[0-9]{5}$" placeholder="Add/tag authors" value="" oninput="search(this.value)" >
                                      <button class="btn btn-outline-secondary dropdown-toggle no-arrow" type="button" data-bs-toggle="dropdown" data-bs-auto-close="false" aria-expanded="false">Search by</button>
                                      <ul class="dropdown-menu">
                                        <li class="dropdown-item">
                                          <input class="search-type" type="radio" id="name" value="name" name='x' checked>
                                          <label>Name</label>
                                        </li>
                                        <li class="dropdown-item">
                                          <input class="search-type" type="radio" id="id" value="id" name='x'>
                                          <label>ID number</label>
                                        </li>
                                      </ul>
                                    </div>
                                    <ul id="search_result" style="list-style: none; padding: 0; display:none; border:.5px solid grey;">
                                    </ul>
                                    <?php
                                      $user_list = $user->getAllStudents();
                                      $stud_list = array();

                                      for ($i=0; $i < sizeof($user_list); $i++) {
                                        if ($user_list[$i]['user_type'] == 'STUDENT') {
                                          array_push($stud_list,$user_list[$i]);
                                        }
                                      }
                                     ?>
                                    <ul class="list-group">

                                      <li id="appendPill" class="list-group-item d-flex-wrap align-items-center"style="color:white;">
                                        <!-- <span class="badge bg-primary rounded-pill">
                                          <h6>&nbsp;&nbsp;email@gmail.com <button class="btn btn-link btn-sm"type="button" name="button"style="color:white;text-decoration:none;">x</button></h6>
                                        </span> -->
                                      </li>
                                    </ul>

                                </div>
                                <div class="form-group">
                                  <input type="file" class="form-control" id="file" name="post_file" onchange="validateFile()" onfocus="validateFile()">
                                  <div id='file_error' class="invalid-feedback"></div>
                                </div>
                                <br>
                                <div align="center">
                                  <input id="submit_btn" type="submit"class="btn btn-primary btn-block" name="add_post" value="Submit">
                                </div>
                                <script type="text/javascript">
                                  var datalistinput = document.getElementById('datalistinput');
                                  var klazz = document.getElementsByClassName('klazz');
                                  var search_type = document.getElementsByClassName('search-type');
                                  var search_result = document.getElementById('search_result');
                                  var stud_list = <?php echo json_encode($stud_list); ?>;

                                  // For client side validation
                                  var title_list = <?php echo json_encode($post->getAllPost()); ?>;
                                  var add_form = document.getElementById('add_form');
                                  var title_input = document.getElementById('title');
                                  var abstract_input = document.getElementById('abstract');
                                  var year_input = document.getElementById('year_publish');
                                  var category_input = document.getElementById('category');
                                  var file_input = document.getElementById('file');

                                  add_form.addEventListener("submit", function (e) {
                                    initialize();

                                    if (title_input.validity.customError || abstract_input.validity.customError || year_input.validity.customError ||
                                        category_input.validity.customError || file_input.validity.customError) {
                                      e.preventDefault();
                                    }
                                  });

                                  function validateTitle() {
                                      title_input.setCustomValidity("");

                                      if (titleExist(sanitizeTitle(title_input.value))) {
                                        title_input.setCustomValidity("&nbsp&nbspThis title is already taken");
                                        document.getElementById('title_error').innerHTML = title_input.validationMessage;
                                      }

                                      const min = 6;
                                      if (title_input.value.length < min) {
                                        title_input.setCustomValidity("&nbsp&nbspTitle should be minimum of 6 characters");
                                        document.getElementById('title_error').innerHTML = title_input.validationMessage;
                                      }

                                      const regex1 = new RegExp("^[A-Za-z0-9:,'\-.( ]+$");
                                      if (!regex1.test(title_input.value)) {
                                        title_input.setCustomValidity("&nbsp&nbspAvailable punctuations :,'\-.(");
                                        document.getElementById('title_error').innerHTML = title_input.validationMessage;
                                      }

                                      const regex2 = new RegExp("^[:,'\-.()]+$");
                                      if (regex2.test(title_input.value)) {
                                        title_input.setCustomValidity("&nbsp&nbspPlease enter a valid title");
                                        document.getElementById('title_error').innerHTML = title_input.validationMessage;
                                      }

                                      const regex3 = new RegExp("^[:,'\-.()0-9]+$");
                                      if (regex3.test(title_input.value)) {
                                        title_input.setCustomValidity("&nbsp&nbspPlease enter a valid title");
                                        document.getElementById('title_error').innerHTML = title_input.validationMessage;
                                      }

                                      if (title_input.validity.customError) {
                                        title_input.setAttribute('class','form-control is-invalid');
                                      }else{
                                        title_input.setAttribute('class','form-control is-valid');
                                      }
                                      // add_form.classList.add('was-validated');
                                  }

                                  function validateAbstract() {
                                      abstract_input.setCustomValidity("");

                                      const min = 6;
                                      if (abstract_input.value.length < min) {
                                        abstract_input.setCustomValidity("&nbsp&nbspAbstract should be minimum of 6 characters");
                                        document.getElementById('abstract_error').innerHTML = abstract_input.validationMessage;
                                      }

                                      const regex1 = new RegExp("^[A-Za-z0-9:,'\-.( ]+$");
                                      if (!regex1.test(abstract_input.value)) {
                                        abstract_input.setCustomValidity("&nbsp&nbspAvailable punctuations :,'\-.(");
                                        document.getElementById('abstract_error').innerHTML = abstract_input.validationMessage;
                                      }

                                      const regex2 = new RegExp("^[:,'\-.()]+$");
                                      if (regex2.test(abstract_input.value)) {
                                        abstract_input.setCustomValidity("&nbsp&nbspPlease enter a valid research abstract");
                                        document.getElementById('abstract_error').innerHTML = abstract_input.validationMessage;
                                      }

                                      const regex3 = new RegExp("^[:,'\-.()0-9]+$");
                                      if (regex3.test(abstract_input.value)) {
                                        abstract_input.setCustomValidity("&nbsp&nbspPlease enter a valid research abstract");
                                        document.getElementById('abstract_error').innerHTML = abstract_input.validationMessage;
                                      }

                                      if (abstract_input.validity.customError) {
                                        abstract_input.setAttribute('class','form-control is-invalid');
                                      }else{
                                        abstract_input.setAttribute('class','form-control is-valid');
                                      }
                                  }

                                  function validateYear() {
                                      year_input.setCustomValidity("");

                                      const regex1 = new RegExp("^[0-9]{4}$");
                                      if (!regex1.test(year_input.value)) {
                                        year_input.setCustomValidity("&nbsp&nbspPlease enter a valid year");
                                        document.getElementById('year_error').innerHTML = year_input.validationMessage;
                                      }

                                      if (year_input.validity.customError) {
                                        year_input.setAttribute('class','form-control is-invalid');
                                      }else{
                                        year_input.setAttribute('class','form-control is-valid');
                                      }
                                      // add_form.classList.add('was-validated');
                                  }

                                  function validateCategory() {
                                      category_input.setCustomValidity("");

                                      const regex1 = new RegExp("^(?:[1-9][0-9]+|[1-9]+)$");
                                      if (!regex1.test(category_input.value)) {
                                        category_input.setCustomValidity("&nbsp&nbspPlease select a research category");
                                        document.getElementById('category_error').innerHTML = category_input.validationMessage;
                                      }

                                      if (category_input.validity.customError) {
                                        category_input.setAttribute('class','form-control is-invalid');
                                      }else{
                                        category_input.setAttribute('class','form-control is-valid');
                                      }

                                    }

                                    function validateFile() {
                                        file_input.setCustomValidity("");
                                        console.log(file_input.files[0]);
                                        if (file_input.files[0] != undefined) {
                                          var ext = file_input.files[0].name.split('.');

                                          // Check for file type
                                          if (ext[ext.length-1] != 'pdf') {
                                            file_input.setCustomValidity("&nbsp&nbspPlease upload a valid PDF file");
                                            document.getElementById('file_error').innerHTML = file_input.validationMessage;
                                          }

                                          // Check file size
                                          if (file_input.files[0].size > 10000000) {
                                            file_input.setCustomValidity("&nbsp&nbspFile size must not exceed to 10mb");
                                            document.getElementById('file_error').innerHTML = file_input.validationMessage;
                                          }
                                        }else{
                                          file_input.setCustomValidity("&nbsp&nbspNo PDF file selected");
                                          document.getElementById('file_error').innerHTML = file_input.validationMessage;
                                        }

                                        if (file_input.validity.customError) {
                                          file_input.setAttribute('class','form-control is-invalid');
                                        }else{
                                          file_input.setAttribute('class','form-control is-valid');
                                        }

                                      }

                                    function initialize(){
                                      validateTitle();
                                      validateAbstract();
                                      validateYear();
                                      validateCategory();
                                      validateFile();
                                    }

                                    function sanitizeTitle(title) {
                                      title = title.toLowerCase();
                                      title = title.replaceAll(/\s/g,"");

                                      return title;
                                    }

                                    function titleExist(title) {
                                      for (var i=0; i < title_list.length; i++) {
                                        title_list[i]['title'] = sanitizeTitle(title_list[i]['title']);
                                        if(title_list[i]['title'] == title){
                                          return true;
                                        }
                                      }
                                      return false;
                                    }

                                    function search(string) {
                                     var to_return = [];
                                     var counter = 0;
                                     var temp = "";
                                     var index_type = (search_type[0].checked) ? "full_name" : "id_number" ;
                                     string = string.toLowerCase();

                                     for (var i = 0; i < stud_list.length; i++) {
                                       var to_test = stud_list[i][index_type].toLowerCase();
                                       if (string.length == 1) {
                                         if (string[0]==to_test[0]) {
                                           to_return[counter]=stud_list[i];
                                           counter++;
                                         }
                                       }else{
                                         if (string.length != 0) {
                                           for (var x = 0; x < to_test.length; x++) {
                                             if (to_test.includes(string)) {
                                               to_return[counter]=stud_list[i];
                                               counter++;
                                               break;
                                             }
                                           }
                                         }
                                       }
                                     }

                                       //Display names
                                     if (to_return.length != 0) {
                                       while(search_result.firstChild){
                                         search_result.removeChild(search_result.firstChild);
                                       }
                                       for (var i = 0; i < to_return.length; i++) {
                                         var id_num = to_return[i]['id_number'].split("-");
                                         var toPass = '"'+id_num[0].toString()+"x"+id_num[1].toString()+'"';
                                         console.log(toPass);
                                         // console.log(toPass);
                                         var new_li = document.createElement('li');
                                         var new_a = document.createElement('a');
                                         new_a.setAttribute('type','button');
                                         new_a.setAttribute('class','dropdown-item');
                                         new_a.setAttribute('onclick','select('+toPass+')');
                                         new_a.innerHTML = to_return[i]['id_number'] + " | " + to_return[i]['full_name'];
                                         search_result.appendChild(new_li);
                                         new_li.appendChild(new_a);
                                       }
                                       search_result.style.display = "block";
                                     }else{
                                       while(search_result.firstChild){
                                         search_result.removeChild(search_result.firstChild);
                                       }
                                       search_result.style.display = "none";
                                     }
                                   }

                                   function select(id_number) {
                                     console.log(id_number);
                                     var student;

                                     id_number = id_number.toString().replace('x','-');
                                     var pills = document.getElementsByClassName(id_number);
                                     // get student info
                                     for (var i = 0; i < stud_list.length; i++) {
                                       if (stud_list[i]['id_number'] == id_number) {
                                         student = stud_list[i];
                                       }
                                     }

                                     if (pills.length == 0) {
                                       var hiddenField = document.createElement("input");
                                       hiddenField.setAttribute("type","hidden");
                                       hiddenField.setAttribute("class",id_number+" klazz");
                                       hiddenField.setAttribute("name","authors[]");
                                       hiddenField.setAttribute("value",id_number);
                                       document.getElementById('datalistgroup').appendChild(hiddenField);
                                       var spanWrapper = document.createElement("span");
                                       var h6 = document.createElement("h6");
                                       var closeButton = document.createElement("button");
                                       //Span element
                                       spanWrapper.setAttribute("class", "badge bg-primary rounded-pill " + id_number);
                                       spanWrapper.setAttribute("style", "margin-right:2px; margin-bottom:1px;");
                                       document.getElementById("appendPill").appendChild(spanWrapper);
                                       //h6
                                       closeButton.setAttribute("class","btn btn-link btn-sm");
                                       closeButton.setAttribute("type","button");
                                       closeButton.setAttribute("style","color:white;text-decoration:none;");
                                       closeButton.setAttribute("id", id_number);
                                       closeButton.setAttribute("onclick", "removeSelected(this.id)");
                                       closeButton.innerHTML = "x";

                                       h6.innerHTML = "&nbsp;&nbsp;"+ student['full_name'];
                                       spanWrapper.appendChild(h6);
                                       h6.appendChild(closeButton);
                                     }
                                     //Hide search results
                                     while(search_result.firstChild){
                                       search_result.removeChild(search_result.firstChild);
                                     }
                                     search_result.style.display = "none";
                                     datalistinput.value = '';
                                   }

                                   function removeSelected(emailRemove) {
                                     console.log("class: "+emailRemove);
                                     for (var i = 0; i < klazz.length; i++) {
                                       // console.log("klazz: "+klazz.value);
                                       if(klazz[i].value == emailRemove){
                                         klazz[i].remove();
                                       }
                                     }
                                     var toRemove = document.getElementsByClassName(emailRemove);
                                     for (var i = 0; i < toRemove.length; i++) {
                                       toRemove[i].remove();
                                      }
                                    }

                              </script>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; CIC Journal System 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="../logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../assets/js/sb-admin-2.min.js"></script>

</body>

</html>

<?php
if (isset($_POST['add_post'])) {
  require '../modal.php';
  array_pop($_POST);
  unset($_POST['x']);

  if ($post->newPost($_POST,$_FILES)) {
    if ($setting->addNewActivity(array($_SESSION['id_number'],"ADDED NEW PAPER"), $_SESSION['user_type'])) {
      echo "<script>
        $(document).ready(function(){
          $('.modal-header').css({
            backgroundColor : '#800000',
            color : 'white',
            textAlign: 'center'
          });
          $('.modal-title').text('CICjournal');
          $('.modal-body').text('Successfuly added paper');
          $('#modal-success-save').hide();
          $('#modal-success-close').hide();
          $('#successModal').modal('show');
          setTimeout(function(){
            $('#successModal').modal('hide');
            window.location.href='managepap.php';
          }, 2000);
        });
      </script>";
    }
  }else{
    require '../modal.php';
    $error = $post->getError();
    echo "<script>
      $(document).ready(function(){
        $('.modal-header').css({
          backgroundColor : '#800000',
          color : 'white',
          textAlign: 'center'
        });
        $('.modal-title').text('CICjournal');
        $('.modal-body').text('$error');
        $('#modal-success-save').hide();
        $('#modal-success-close').hide();
        $('#successModal').modal('show');
        setTimeout(function(){
          $('#successModal').modal('hide');
          window.location.href='managepap.php';
        }, 2000);
      });
    </script>";
  }

}

 ?>
