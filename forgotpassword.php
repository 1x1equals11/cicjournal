<?php
  require 'database/db_connect.php';
  require 'database/db.php';
  require 'controllers/user_dashboard_controller.php';
  require 'controllers/login_controller.php';
  require 'controllers/settings_controller.php';
  session_start();

  if(isset($_SESSION['id_number']) && $_SESSION['user_type'] == "SUPER"){
    header("location: admin/");
  }elseif (isset($_SESSION['id_number']) && $_SESSION['user_type'] == "STUDENT") {
    header("location: student/");
  }

  $forgot = new Login();
  $user = new UserDashboard("","");
  $error = "";

 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="images/ciclogomain.png" type="image/png" />

    <title>Forgot password</title>

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

</head>

<body id="page-top" zoom="150">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
              <div class=" bg-gradient-primary mb-5 p-3">
                <div class="container">
                  <a class="row" href="index.php">
                    <div class="col-12 text-center">
                      <img src="images/ciclogo.png" class="img-fluid logo-width">
                    </div>
                  </a>
                </div>
              </div>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
              <div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="p-3">
                      <form class="user" method="post">

                        <div class="form-group row fp-md-center">
                          <div class="col-lg-7">
                            <div class="mb-5">
                              <h1 class="display-4 text-gray-900 mb-4 fp-md-fs-title">Forgot Password?</h1>
                              <p class="text-gray-600 fp-semi-title-size fp-md-fs-semi">
                                Please enter your information below to reset your password
                                After successful reset, your new password will be your birth date.
                              </p>
                            </div>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-lg-6">
                            <input type="text" class="form-control text-capitalize" name="middle_name" placeholder="Middle Name" required>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-lg-6">
                            <input type="password" class="form-control" name="id_number" placeholder="Enter ID No" required>
                          </div>
                        </div>

                        <div class="form-group row mb-4">
                          <div class="col-lg-6">
                            <input type="text" class="form-control" name="dob" placeholder="Date of Birth (yyyy-mm-dd)" required>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group row text-center">
                            <div class="col-lg-4 p-2 mr-3">
                              <button type="submit" class="btn btn-primary btn-user btn-block" name="reset">Reset password</button>
                            </div>
                            <div class="col-lg-auto p-3 mr-2">
                              or
                            </div>
                            <div class="col-lg-auto p-3 text-center fs-fp-link">
                              <a href="login.php" class="text-gray-800 text-decoration-none">Login instead</a>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <script type="text/javascript">

                    </script>
                  </div>
                </div>
              </div>
              <!-- /.container -->


          </div>
          <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; CIC Journal 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <script type="text/javascript"></script>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>
</html>
<?php
if (isset($_POST['reset'])) {
  require 'modal.php';
  array_pop($_POST);
  $to_pass = $user->getSpecificUser($_POST['id_number']);

  if ($forgot->forgotPassword($_POST,$to_pass)) {
    echo "<script>
      $(document).ready(function(){
        $('.modal-header').css({
          backgroundColor : '#800000',
          color : 'white',
          textAlign: 'center'
        });
        $('.modal-title').text('CICjournal');
        $('.modal-body').text('Success. You may now login with your new password');
        $('#modal-success-save').hide();
        $('#modal-success-close').hide();
        $('#successModal').modal('show');
        setTimeout(function(){
          $('#successModal').modal('hide');
          window.location.href='login.php';
        }, 2000);
      });
    </script>";
  }else{
    $error = $forgot->getError();
    echo "<script>
      $(document).ready(function(){
        $('.modal-header').css({
          backgroundColor : '#800000',
          color : 'white',
          textAlign: 'center'
        });
        $('.modal-title').text('CICjournal');
        $('.modal-body').text('$error');
        $('#modal-success-save').hide();
        $('#modal-success-close').hide();
        $('#successModal').modal('show');
        setTimeout(function(){
          $('#successModal').modal('hide');
          window.location.href='login.php';
        }, 2000);
      });
    </script>";
  }
}
 ?>
