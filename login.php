<?php
  require 'database/db_connect.php';
  require 'database/db.php';
  require 'controllers/login_controller.php';
  require 'controllers/settings_controller.php';
  session_start();

  if(isset($_SESSION['id_number']) && $_SESSION['user_type'] == "SUPER"){
    header("location: admin/");
  }elseif (isset($_SESSION['id_number']) && $_SESSION['user_type'] == "STUDENT") {
    header("location: student/");
  }
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="images/ciclogomain.png" type="image/png" />

    <title>Login</title>

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

</head>

<body id="page-top" zoom="150">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
              <div class=" bg-gradient-primary mb-5 p-3">
                <div class="container">
                  <a class="row" href="/cicjournal/">
                    <div class="col-12 text-center">
                      <img src="images/ciclogo.png" class="img-fluid logo-width">
                    </div>
                  </a>
                </div>
              </div>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
              <div class="container mb-5">
                <div class="row">
                  <div class="col-xs-6 col-lg-6 p-5 text-center">
                    <img src="images/poster5.png" class="img-fluid" width="365">
                  </div>
                  <div class="col-xs-6 col-lg-6">
                    <div class="p-5">
                      <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Credentials</h1>
                      </div>
                      <form class="user" method="post">
                        <div class="form-group">
                          <input type="text" class="form-control form-control" name="id_number" placeholder="Enter ID No.">
                        </div>
                        <div class="form-group">
                          <input type="password" class="form-control form-control" id="exampleInputPassword" name="password" placeholder="Password">
                        </div>

                        <div class="row text-center">
                          <div class="col-lg-12 p-2 mr-3">
                            <button type="submit" class="btn btn-primary btn-user btn-block" name="login">Log me in</button>
                          </div>
                        </div>
                      </form>
                      <hr>
                      <div class="text-center">
                        <a class="small" href="forgotpassword.php">Forgot Password?</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.container -->

          </div>
          <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; CIC Journal 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>
<?php
  if(isset($_POST['login'])){
    require 'modal.php';
    $login = new Login();
    $setting = new Settings();
    array_pop($_POST);

    if($login->newLogin($_POST)){

      $directory = ($login->getUserType() == "STUDENT" ? "student/" : "admin/");
      $_SESSION['user_type'] = $login->getUserType();
      $_SESSION['id_number'] = $_POST['id_number'];

      if ($setting->addNewActivity(array($_SESSION['id_number'],'LOGGED IN'),$_SESSION['user_type'])) {
        echo "<script>
          $(document).ready(function(){
            $('.modal-header').css({
              backgroundColor : '#800000',
              color : 'white',
              textAlign: 'center'
            });
            $('.modal-title').text('CICjournal');
            $('.modal-body').text('Welcome admin');
            $('#modal-success-save').hide();
            $('#modal-success-close').hide();
            $('#successModal').modal('show');

            setTimeout(function(){
              $('#successModal').modal('hide');
              window.location.href='$directory';
            }, 2000);
            });
        </script>";
      }else{
        echo "<script>
          $(document).ready(function(){
            $('.modal-header').css({
              backgroundColor : '#800000',
              color : 'white',
              textAlign: 'center'
            });
            $('.modal-title').text('CICjournal');
            $('.modal-body').text('Welcome');
            $('#modal-success-save').hide();
            $('#modal-success-close').hide();
            $('#successModal').modal('show');
            setTimeout(function(){
              $('#successModal').modal('hide');
              window.location.href='$directory';
            }, 2000);
          });
        </script>";
      }
    }else{
      require 'modal.php';
      echo "<script>
        $(document).ready(function(){

          $('.modal-header').css({
            backgroundColor : '#800000',
            color : 'white',
            textAlign: 'center'
          });
          $('.modal-title').text('CICjournal');
          $('.modal-body').text('Incorrect ID number or password');
          $('#modal-success-save').hide();
          $('#modal-success-close').hide();
          $('#successModal').modal('show');
          setTimeout(function(){
            $('#successModal').modal('hide');
            window.location.href = 'login.php';
          }, 4000);
        });
      </script>";
    }
  }
 ?>
