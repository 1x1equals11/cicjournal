<?php
  require 'database/db_connect.php';
  require 'database/db.php';
  require 'controllers/login_controller.php';
  require 'controllers/settings_controller.php';
  session_start();

  if(isset($_SESSION['id_number']) && $_SESSION['user_type'] == "SUPER"){
    header("location: admin/");
  }elseif (isset($_SESSION['id_number']) && $_SESSION['user_type'] == "STUDENT") {
    header("location: student/");
  }

  $signup = new Login();
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

</head>

<body id="page-top" zoom="150">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
              <div class=" bg-gradient-primary mb-5 p-3">
                <div class="container">
                  <a class="row" href="index.php">
                    <div class="col-12 text-center">
                      <img src="images/ciclogo.png" class="img-fluid logo-width">
                    </div>
                  </a>
                </div>
              </div>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
              <div class="container mb-5">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="p-3">
                      <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                      </div>
                      <form class="user">
                        <div class="form-group row">
                          <div class="col-sm-4 mb-3 ">
                            <input type="text" class="form-control" id="#" placeholder="First Name">
                          </div>
                          <div class="col-sm-4 mb-3">
                            <input type="text" class="form-control" id="#" placeholder="Middle Name">
                          </div>
                          <div class="col-sm-4">
                            <input type="text" class="form-control" id="#" placeholder="Last Name">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-6 mb-3">
                            <input placeholder="Date of Birth" class="textbox-n form-control" type="text" onfocus="(this.type='date')" id="date">
                          </div>
                          <div class="col-sm-6">
                            <select class="form-control" name="user_profile[]">
                              <option disabled="" selected="">Sex</option>
                              <option value="MALE">Male</option>
                              <option value="FEMALE">Female</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-sm-6 mb-3">
                            <input type="password" class="form-control" id="exampleInputPassword" placeholder="Enter ID No">
                          </div>
                          <div class="col-sm-6">
                            <input type="password" class="form-control" id="exampleInputPassword" placeholder="Enter Password">
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-sm-6 mb-3">
                            <input type="password" class="form-control" id="exampleRepeatPassword" placeholder="Repeat Password">
                          </div>
                          <div class="col-sm text-center">
                            <a href="#" class="btn btn-primary btn-user btn-block">
                              Register Account
                            </a>
                          </div>
                        </div>

                      </form>
                      <hr>
                      <div class="text-center">
                        <a class="small" href="#">Forgot Password?</a>
                      </div>
                      <div class="text-center">
                        <a class="small" href="login.php">Already have an account? Login!</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.container -->

          </div>
          <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; CIC Journal 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>
