<?php
  require 'database/db_connect.php';
  require 'database/db.php';
  require 'controllers/login_controller.php';
  require 'controllers/settings_controller.php';
  require 'controllers/posts_controller.php';
  require 'controllers/user_dashboard_controller.php';
  session_start();

  if (isset($_SESSION['id_number'])) {
    if ($_SESSION['user_type'] == "SUPER") {
      header("location: admin/");
    }elseif ($_SESSION['user_type'] == "STUDENT") {
      header("location: student/");
    }
  }elseif (!isset($_GET['post_id'])) {
    header("location: index.php");
  }

  $login = new Login();
  $setting = new Settings();
  $user = new UserDashboard("","");
  $post = new Posts("","");
  $view_post = $post->getSpecificPaper($_GET['post_id']);
  $likes = $post->getReactionCount($_GET['post_id'], "LIKE");
  $unlikes = $post->getReactionCount($_GET['post_id'], "UNLIKE");
  $comments = $post->getComments($_GET['post_id']);
 ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="images/ciclogomain.png" type="image/png" />

    <title><?php echo $view_post['title']; ?></title>

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/custom.css">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content just">
          <!-- Topbar -->
          <nav class="navbar navbar-expand-lg navbar-light bg-gradient-primary mb-5">
            <div class="container">
              <a class="navbar-brand" href="/cicjournal/">
                 <img src="images/ciclogo.png" class="img-fluid logo-width">
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <div class="navbar-nav mr-auto"></div>
              <span class="form-inline">
                  <a href="login.php" class="zoom text-white nav-item nav-link">Sign in</a>
              </span>
            </div>
            </div>
          </nav>
          <!-- end of Topbar -->

          <!-- Begin Page Content -->
          <div class="container">
            <div class="ml-4">
                <div class="text-info">
                    <p class="small mb-2">
                      <?php
                        $date = date_create($view_post['date_posted']);
                        $date2 = date_format($date, "jS F, Y i:sa ");
                        echo $date2;
                      ?>
                        <!-- 28th January, 3:48pm -->
                    </p>
                     <h1 class="h3 mb-2"><?php echo $view_post['title']; ?></h1>

                </div>
                <div class="mb-2">
                    <code>Author/s</code>
                </div>
                <div class="row">
                  <?php $authors = $post->getPaperAuthors($view_post['post_id']); ?>
                   <?php if (!empty($authors)): ?>
                     <?php foreach ($authors as $value): ?>
                       <?php
                        $author = $user->getSpecificUser($value);
                        $default_avatar = ($author['sex'] == 'MALE'? 'male_default.jpg' : 'female_default.jpg' );
                        $author['prof_pic'] = ($author['prof_pic'] != null ? $author['prof_pic'] : $default_avatar);
                       ?>
                       <div class="d-flex align-items-center mr-4 mb-4">
                           <div class="mr-2">
                               <img class="icon_img rounded-circle" src="profile_pictures/<?php echo $author['prof_pic']; ?>" width="40px" style="object-fit:cover;">
                           </div>
                           <div class="font-weight-bold">
                               <div class=""><?php echo $author['last_name'].", ".$author['first_name'] ?></div>
                           </div>
                       </div>
                     <?php endforeach; ?>
                   <?php else: ?>
                     <i>Authors on paper</i>
                   <?php endif; ?>

                </div>
            </div>
          </div>

          <div class="container">
              <div class="row">
                  <div class="col-lg-12 p-3">

                    <div class="d-sm-flex align-items-center justify-content-end">
                        <a href="login.php" class="d-none d-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download File</a>
                    </div>
                      <hr>
                      <h3 class="mb-4">Abstract</h3>
                      <p class="">
                          <?php echo $view_post['synopsis']; ?>
                      </p>
                      <hr>

                      <hr>
                      <div id="comment_section" style="margin:1%;">
                        <?php if (!empty($comments)): ?>
                          <?php foreach ($comments as $comment): ?>
                            <?php
                              $keyboard_warrior = $user->getSpecificUser($comment['id_number']);
                              $default_avatar = ($keyboard_warrior['sex'] == 'MALE'? 'male_default.jpg' : 'female_default.jpg' );
                              $keyboard_warrior['prof_pic'] = ($keyboard_warrior['prof_pic'] != null ? $keyboard_warrior['prof_pic'] : $default_avatar);
                            ?>
                          <div class="card p-2">
                            <div class="d-flex mr-4 mb-4 ">
                              <form method="post" id='<?php echo $comment['id']; ?>' style="display:none;">
                                <div class="d-flex">
                                  <div class="mr-1">
                                      <a href="#"><img class="icon_img rounded-circle" src="profile_pictures/<?php echo $keyboard_warrior['prof_pic']; ?>" alt="" width="40px"></a>
                                  </div>
                                  <div class="font-weight-bold">

                                    <a href="#"><div class=""><?php echo $keyboard_warrior['first_name']." ".$keyboard_warrior['last_name']; ?></div></a>
                                    <textarea class="form-control form-control-sm" name="user_comment" rows="3" cols="40" ><?php echo stripslashes($comment['comment']); ?></textarea> <br>
                                    <div class="d-sm-flex"style="width:100%;">
                                      <input type="hidden" name="id" value="<?php echo $comment['id']; ?>">
                                      <input class="btn btn-sm btn-primary col-lg-6 mr-2 mb-1 mt-1" type="submit" name="edit_comment" value="Confirm edit">
                                      <button class="btn btn-sm btn-success col-lg-6 mt-1 mb-1" type="button" onclick='revert(<?php echo $comment['id']; ?>)'>Cancel</button>
                                    </div>
                                  </div>
                                </div>

                              </form>
                              <div class="d-flex" id='user_comment_<?php echo $comment['id']; ?>'>
                                <div class="mr-2">
                                    <div href="profile.php?id=<?php echo $keyboard_warrior['id']; ?>"><img class="icon_img rounded-circle" src="profile_pictures/<?php echo $keyboard_warrior['prof_pic']; ?>" alt="" width="40px"></div>
                                </div>
                                <div class="font-weight-bold">
                                  <?php //$keyboard_warrior = $user->getSpecificUser($comment['id_number']); ?>
                                    <div href="profile.php?id=<?php echo $keyboard_warrior['id']; ?>"><div class='mb-3' style='color:black;'><?php echo $keyboard_warrior['first_name']." ".$keyboard_warrior['last_name']; ?></div></div>
                                    <?php echo "<span >".stripslashes($comment['comment'])."</span>"; ?>
                                </div>
                              </div>

                            </div>
                            <div id='comment_date_<?php echo $comment['id']; ?>'>
                              <small><i><?php echo $comment['comment_date']; ?></i></small>
                            </div>

                            <script type="text/javascript">
                              var editButton = document.getElementById('edit_button');
                              var editForm = document.getElementById('hidden_form');
                              var userComment = document.getElementById('user_comment');
                              var setting = document.getElementById('setting');
                              var commentDate = document.getElementById('comment_date');

                              function transform(x) {
                                // edit
                                var editForm = document.getElementById(x);
                                var userComment = document.getElementById('user_comment_'+x);
                                var setting = document.getElementById('setting_'+x);
                                var commentDate = document.getElementById('comment_date_'+x);
                                userComment.style.visibility = 'hidden';
                                setting.style.visibility = 'hidden';
                                commentDate.style.visibility = 'hidden';
                                editForm.style.display = 'block';
                              }

                              function revert(x) {
                                var editForm = document.getElementById(x);
                                var userComment = document.getElementById('user_comment_'+x);
                                var setting = document.getElementById('setting_'+x);
                                var commentDate = document.getElementById('comment_date_'+x);
                                userComment.style.visibility = 'visible';
                                setting.style.visibility = 'visible';
                                commentDate.style.visibility = 'visible';
                                editForm.style.display = 'none';
                              }

                            </script>
                          </div>
                          <br>
                        <?php endforeach; ?>
                        <?php else: ?>
                          <i>No comments</i>
                        <?php endif; ?>
                      </div>
                      <hr>
                      <div id="write_comment_section"style="margin:1%;">
                        <form method="post">
                          <input type="hidden" name="post_id" value="<?php echo $_GET['post_id']; ?>">
                          <div class="form-group">
                            <input class="form-control form-control-sm" id="comment_str" type="text" name="comment_str" placeholder="Please login to write a comment" disabled>
                            <input type="submit" id="to_enter"name="comment" value="comment" hidden>
                          </div>
                        </form>
                      </div>
                  </div>


              </div>
          </div>

          <!-- Footer -->
          <footer class="sticky-footer bg-white">
              <div class="container my-auto">
                  <div class="copyright text-center my-auto">
                      <span>Copyright &copy; CIC Journal 2021</span>
                  </div>
              </div>
          </footer>
          <!-- End of Footer -->

      </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>
